from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView

from .marcacion.views import MarcacionViewSet
from .sincronizacion.views import (
    SincronizarTodosDispositivoViewSet,
    ActualizarInfoDispositivoViewSet,
    SincronizarSoftloftViewSet,
    SincronizarSoftmantViewSet,
    SincronizarDispositivoViewSet,
    MigrarOrdenesSoftflotOdooViewSet
)

router = DefaultRouter()
router.register(r'marcacion', MarcacionViewSet, basename='marcacion')


urlpatterns = [
    path('', include((router.urls, 'api'))),
    path('auth/token/', TokenObtainPairView.as_view(), name="auth-token"),
    path('sincronizar/', SincronizarTodosDispositivoViewSet.as_view(), name="sincronizar-dispositivos"),
    path('sincronizar/dispositivo/', SincronizarDispositivoViewSet.as_view(), name="sincronizar-dispositivo"),
    path('actualizar/info/', ActualizarInfoDispositivoViewSet.as_view(), name="actualizar-info-dispositivo"),
    path('sincronizar/softflot/', SincronizarSoftloftViewSet.as_view(), name="sincronizar-softflot"),
    path('sincronizar/softfmant/', SincronizarSoftmantViewSet.as_view(), name="sincronizar-softmant"),
    path('sincronizar/softflot/orden/', MigrarOrdenesSoftflotOdooViewSet.as_view(), name="sincronizar-softflot-ordenes"),
]

