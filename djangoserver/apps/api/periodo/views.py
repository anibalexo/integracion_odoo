from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import PeriodoSerializer
from ...biometrico.models.periodo import Periodo


class PeriodoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Endpoint para periodos
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, )

    queryset = Periodo.objects.all()
    serializer_class = PeriodoSerializer
