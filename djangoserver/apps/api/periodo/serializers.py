from rest_framework import serializers

from ...biometrico.models.periodo import Periodo


class PeriodoSerializer(serializers.ModelSerializer):
    """
    Serializer para listar periodos
    """
    class Meta:
        model = Periodo
        fields = ('pk', 'fecha_inicial', 'fecha_final',)