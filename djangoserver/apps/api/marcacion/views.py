import datetime

from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import MarcacionSerializer
from ...biometrico.models import Marcacion
from ...utils.pagination import CustomPagination


class MarcacionViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Endpoint para consultar marcaciones
    en dispositivos biometricos
    """
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated, )
    pagination_class = CustomPagination
    serializer_class = MarcacionSerializer

    def get_queryset(self):
        queryset = Marcacion.objects.all()
        return queryset

    def list(self, request):
        """
        Listar todos las marcaciones
        :return: Response
        """
        date_from = request.query_params.get('from', None)
        date_to = request.query_params.get('to', None)
        if date_from is not None and date_to is not None:
            fecha_inicial_dt = datetime.datetime.strptime(date_from, '%Y-%m-%d')
            fecha_final_dt = datetime.datetime.strptime(date_to, '%Y-%m-%d')
            queryset = Marcacion.objects.filter(fecha__range=(fecha_inicial_dt, fecha_final_dt.replace(hour=23, minute=59)))
        else:
            if "page" in request.query_params:
                marcaciones = Marcacion.objects.all()
                queryset = self.paginate_queryset(marcaciones)
                serializer = MarcacionSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)
            else:
                return Response({"No ha parametros"}, status=status.HTTP_400_BAD_REQUEST)
        serializer = MarcacionSerializer(queryset, many=True, context={"request": request})
        return Response(serializer.data)
