from rest_framework import serializers

from ...biometrico.models import Marcacion


class MarcacionSerializer(serializers.ModelSerializer):
    """
    Serializer para listar marcaciones
    """
    id_oddo = serializers.SerializerMethodField()

    class Meta:
        model = Marcacion
        fields = (
            'pk',
            'fecha',
            'dispositivo',
            'id_oddo'
        )

    def get_id_oddo(self, obj):
        """
        Retorna el id de empleado en odoo"
        :param obj: instancia de empleado
        :return: int: id de empleado en erp odoo
        """
        return obj.empleado.id_odoo
