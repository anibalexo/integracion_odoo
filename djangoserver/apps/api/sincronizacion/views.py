from rest_framework import views, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from ...biometrico.models import Dispositivo
from ...biometrico.utils import sincronizar_empleados, sincronizar_marcaciones, actualizar_info_dispositivo
from ...biometrico.zk_py import const
from ...biometrico.zk_py.exception import ZKError
from ...softflot.utils.softflot_utils import actualizar_empleados, actualizar_vehiculos, actualizar_materiales, actualizar_ordenes
from ...softflot.utils.odoo_utils import obtener_dict_ordenes_softflot
from ...softmant.utils.softmant_utils import actualizar_empleados_sfm, actualizar_materiales_sfm, actualizar_elementos_sfm, actualizar_ordenes_sfm


def get_privilegio(user):
    privilege = "User"
    if user.privilege == const.USER_ADMIN:
        privilege = 'Admin'
    return privilege


class SincronizarTodosDispositivoViewSet(views.APIView):
    """
    Sincronizamos registros de todos los dispositivos
    biometricos a base de integración
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        dispositivos = Dispositivo.objects.filter(activo=True)
        rsp = []
        for dsp in dispositivos:
            try:
                sincronizar_empleados(dsp)
                sincronizar_marcaciones(dsp)
                rsp.append({'estado': 'ok', 'descripcion': 'Sincronización realizada con éxito', 'dispositivo': dsp.nombre})
            except ZKError as e:
                rsp.append({'estado': 'error', 'descripcion': str(e), 'dispositivo': dsp.nombre})
        return Response(rsp, status.HTTP_200_OK)


class ActualizarInfoDispositivoViewSet(views.APIView):
    """
    Consultamos y Actualizamos informacion
    de un dispositivo
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        dispositivo_id = self.request.query_params.get('dispositivo_id', None)
        if dispositivo_id is None:
            return Response({'error': 'No se encontro el id dispositivo'}, status.HTTP_400_BAD_REQUEST)
        rsp = []
        try:
            dispositivo = Dispositivo.objects.get(pk=dispositivo_id)
            actualizar_info_dispositivo(dispositivo)
            rsp.append({'estado': 'ok', 'descripcion': 'Sincronización realizada con éxito', 'dispositivo': dispositivo.nombre})
        except ZKError as e:
            rsp.append({'estado': 'error', 'descripcion': str(e), 'dispositivo': dispositivo.nombre})
        return Response(rsp, status.HTTP_200_OK)


class SincronizarSoftloftViewSet(views.APIView):
    """
    Sincronizamos ordenes de softflot
    a base de integración
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        actualizar_empleados()
        actualizar_vehiculos()
        actualizar_materiales()
        actualizar_ordenes()
        result = {'estado': 'ok'}
        return Response(result, status.HTTP_200_OK)


class SincronizarSoftmantViewSet(views.APIView):
    """
    Sincronizamos ordenes de softmant
    a base de integración
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        actualizar_empleados_sfm()
        actualizar_elementos_sfm()
        actualizar_materiales_sfm()
        actualizar_ordenes_sfm()
        result = {'estado': 'ok'}
        return Response(result, status.HTTP_200_OK)


class SincronizarDispositivoViewSet(views.APIView):
    """
    Sincroniza informacion de un dispositivo
    a base de integracion odoo
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        dispositivo_id = self.request.query_params.get('dispositivo_id', None)
        if dispositivo_id is None:
            return Response({'error': 'No se envio el id dispositivo'}, status.HTTP_400_BAD_REQUEST)
        rsp = []
        try:
            dispositivo = Dispositivo.objects.get(pk=dispositivo_id)
            sincronizar_empleados(dispositivo)
            sincronizar_marcaciones(dispositivo)
            rsp.append({'estado': 'ok', 'descripcion': 'Sincronización realizada con éxito', 'dispositivo': dispositivo.nombre})
        except ZKError as e:
            rsp.append({'estado': 'error', 'descripcion': str(e), 'dispositivo': dispositivo.nombre})
        return Response(rsp, status.HTTP_200_OK)


class MigrarOrdenesSoftflotOdooViewSet(views.APIView):
    """
    Migra ordenes de softflot desde base
    de integracion a ERP Odoo
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        lista_ordenes = obtener_dict_ordenes_softflot()
        for ord in lista_ordenes:
            if ord['employee_id'] != 0:
                print(ord)
            result = {'estado': 'ok'}
        return Response(result, status.HTTP_200_OK)