""" Configuracion del Router para verificar 
    entre multiples bases de datos"""


class ConfigRouterAuth(object):
    """
    Router de control de lectura y escritura
    de la base de datos default para la app auth.
    """
    def db_for_read(self, model, **hints):
        """
        Verificación de aplicaciones que solo
        requieren lectura de la base de datos default
        """
        if model._meta.app_label == "auth":
            return 'default'
        return None
    
    def db_for_write(self, model, **hints):
        """
        Escribir cualquier transacción en default
        """
        if model._meta.app_label == 'auth':
            return 'default'
        return None
    
    def allow_relation(self, obj1, obj2, **hints):
        """
        Permite la relación si los objetos son de la app
        auth.
        """
        if obj1._meta.app_label == 'auth' or \
           obj2._meta.app_label == 'auth':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Asegura que la app auth solo aparezca
        en la base de datos default
        """
        if app_label in DATABASE_APPS_MAPPING:
            return db == 'default'
        return None


DATABASE_APPS_MAPPING = ['contenttypes', 'auth',
                         'admin', 'sessions', 'sites',
                         'messages', 'staticfiles'
                         'usuario', 'allauth', 'softflot',]