from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import DashboardTemplateView

urlpatterns = [
    path('', login_required(DashboardTemplateView.as_view()), name='dashboard-home'),
]