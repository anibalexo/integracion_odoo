from django.contrib import admin
from .models import Empleado, Material, DetalleMaterial, DetalleTarea, ElementoOrden, Orden, Elemento


class DetalleMaterialAdmin(admin.ModelAdmin):
    """
    Model admin para detalle de materiales en una orden
    """
    list_display = ('pk', 'cantidad', 'material', 'elemento_orden', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('material__descripcion', 'material__codigo_material')
    list_per_page = 10
    ordering = ('-fecha_creacion',)


class DetalleTareaAdmin(admin.ModelAdmin):
    """
    Model admin para detalle de tareas en un orden
    """
    list_display = ('pk', 'descripcion', 'id_empleado', 'fecha', 'realizada', 'elemento_orden', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('descripcion',)
    list_per_page = 10
    ordering = ('-fecha_creacion',)


class ElementoOrdenAdmin(admin.ModelAdmin):
    """
    Model admin para elementos en orden de softmant
    """
    list_display = ('pk', 'id_elemento', 'id_manto_elemento', 'orden', 'elemento', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('elemento__descripcion',)
    list_per_page = 10
    ordering = ('-fecha_creacion',)


class EmpleadoAdmin(admin.ModelAdmin):
    """
    Modela admin para empleados registrados en softmant
    """
    list_display = ('pk', 'id_odoo', 'id_empleado', 'dni', 'nombres', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('dni', 'nombres')
    list_per_page = 10
    ordering = ('-fecha_creacion',)


class MaterialAdmin(admin.ModelAdmin):
    """
    Model admin para materiales
    """
    list_display = ('pk', 'id_odoo', 'id_material', 'unidad', 'id_unidad_odoo', 'codigo_material', 'descripcion', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('codigo_material', 'descripcion')
    list_per_page = 10
    ordering = ('-fecha_creacion',)


class OrdenAdmin(admin.ModelAdmin):
    """
    Model admin para ordenes de softmant
    """
    list_display = ('pk', 'id_orden', 'empleado', 'tipo', 'folio', 'descripcion', 'fecha', 'duracion', 'registro_odoo', 'registro_odoo_fecha')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('empleado__nombres', 'folio', 'descripcion')
    list_per_page = 10
    ordering = ('-fecha_creacion',)


class ElementoAdmin(admin.ModelAdmin):
    """
    Model admin para elementos de softmant
    """
    list_display = ('pk', 'id_odoo', 'id_elemento', 'id_tipo_elemento', 'nro_economico', 'descripcion', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('nro_economico', 'descripcion', 'codigo_barra')
    list_per_page = 10
    ordering = ('-fecha_creacion',)


admin.site.register(DetalleMaterial, DetalleMaterialAdmin)
admin.site.register(DetalleTarea, DetalleTareaAdmin)
admin.site.register(ElementoOrden, ElementoOrdenAdmin)
admin.site.register(Empleado, EmpleadoAdmin)
admin.site.register(Material, MaterialAdmin)
admin.site.register(Orden, OrdenAdmin)
admin.site.register(Elemento, ElementoAdmin)