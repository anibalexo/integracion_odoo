from django.contrib.auth.decorators import login_required
from django.urls import path
from .views import InicioSoftmantTemplateView, ElementoListView, OrdenListView, EmpleadoListView, MaterialListView, DetalleOrdenDetailView


urlpatterns = [
    path('', login_required(InicioSoftmantTemplateView.as_view()), name='softmant-inicio'),
    path('elemento', login_required(ElementoListView.as_view()), name='softmant-elemento'),
    path('empleado', login_required(EmpleadoListView.as_view()), name='softmant-empleado'),
    path('material', login_required(MaterialListView.as_view()), name='softmant-material'),
    path('orden', login_required(OrdenListView.as_view()), name='softmant-orden'),
    path('orden/<int:pk>', login_required(DetalleOrdenDetailView.as_view()), name='softmant-orden-detalle'),
]
