from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from .models import Elemento, Orden, Empleado, Material


def obtener_registros_no_registrados(lista):
    resultado = 0
    for item in lista:
        if item.id_odoo == 0:
            resultado += 1
    return resultado


def obtener_ordenes_estado(lista):
    subidas = 0
    pendientes = 0
    for item in lista:
        if item.registro_odoo == True:
            subidas += 1
        else:
            pendientes += 1
    return {"subidas": subidas, "pendientes": pendientes}


class InicioSoftmantTemplateView(TemplateView):
    template_name = 'softmant/inicio.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        empleados = Empleado.objects.all()
        context["num_empleados"] = len(empleados)
        context["num_empleados_odoo"] = obtener_registros_no_registrados(empleados)
        elementos = Elemento.objects.all()
        context["num_elementos"] = len(elementos)
        context["num_elementos_odoo"] = obtener_registros_no_registrados(elementos)
        materiales = Material.objects.all()
        context["num_materiales"] = len(materiales)
        context["num_materiales_odoo"] = obtener_registros_no_registrados(materiales)
        ordenes = Orden.objects.all()
        context["num_ordenes"] = len(ordenes)
        context["num_ordenes_estado"] = obtener_ordenes_estado(ordenes)
        return context


class ElementoListView(ListView):
    model = Elemento
    ordering = ('-fecha_creacion',)
    paginate_by = 15
    template_name = 'softmant/elemento_list.html'


class OrdenListView(ListView):
    model = Orden
    ordering = ('-fecha_creacion',)
    paginate_by = 15
    template_name = 'softmant/orden_list.html'


class EmpleadoListView(ListView):
    model = Empleado
    ordering = ('-fecha_creacion',)
    paginate_by = 15
    template_name = 'softmant/empleado_list.html'


class MaterialListView(ListView):
    model = Material
    ordering = ('-fecha_creacion',)
    paginate_by = 15
    template_name = 'softmant/material_list.html'


class DetalleOrdenDetailView(DetailView):
    model = Orden
    template_name = 'softmant/orden_detalle.html'
