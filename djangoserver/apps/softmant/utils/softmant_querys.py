from django.db import connections

from ...incidente import models as modelsIncidente
from ...incidente.models import Notificacion


def dictfetchall(cursor):
    "Retorna filas de cursor como diccionario"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def funcion_ejecutar_softmant(sql):
    """
    Retorna diccionario obtenido al ejecutar un sql
    :param sql: sentencia sql para ejecutar
    :return: diccionario
    """
    cursor = connections['softfmant_db'].cursor()
    try:
        cursor.execute(sql)
        return dictfetchall(cursor)
    except Exception as e:
        cursor.close()
        return False, e
    finally:
        cursor.close()


def obtener_empleados_softmant():
    """Retorna lista de empleados de softmant"""
    sql = """
    SELECT E.Id_Empleado, E.Clave, E.Nombre FROM Cat_Empleados E ORDER BY E.Nombre
    """
    return funcion_ejecutar_softmant(sql)


def obtener_materiales_softmant():
    """Retorna lista materiales de softmant"""
    sql = """
    SELECT M.Id_Material, U.Descripcion AS Unidad, M.Clave_Material, M.Descripcion FROM Cat_Materiales M LEFT JOIN Cat_Unidades U ON M.Id_Unidad = U.Id_Unidad 
    ORDER BY M.Id_Unidad
    """
    return funcion_ejecutar_softmant(sql)


def obtener_elementos_softmant():
    """Retorna lista de elementos de softmant"""
    sql = """
    SELECT E.Id_Elemento, E.Id_TipoDeElemento, E.NoEconomico, E.Descripcion, E.ElementoBarCode FROM Mast_Elementos E WHERE E.Id_TipoDeElemento > 2
    """
    return funcion_ejecutar_softmant(sql)


def obtener_ordenes_softmant():
    """Retorna lista de ordenes de softmant"""
    sql = """
    SELECT M.Id_OTManttoMaster, M.Id_Empleado, S.Descripcion AS EstadoOS, T.Descripcion AS TipoOS, M.Folio, M.Descripcion, convert(nvarchar, M.Fecha, 20) AS Fecha,
    M.Comentarios, M.TiempoRealizacion, E.Localizacion, E.Descripcion AS Localizacion_Descripcion 
    FROM Mov_OTMantto1MasterHist M
    LEFT JOIN Sis_OTStatus S ON M.Id_OTStatus = S.Id_OTStatus 
    LEFT JOIN Sis_TipoOT T ON M.Id_TipoOT = T.Id_TipoOT
    LEFT JOIN Mast_Elementos E ON M.Id_Localizacion = E.Id_Elemento
    WHERE S.Id_OTStatus = 10 AND M.Fecha >= '2021-10-01' ORDER BY M.Fecha DESC    
    """
    return funcion_ejecutar_softmant(sql)


def obtener_elementos_orden(id_orden):
    """
    Retorna los elementos de una orden de softmant
    :param id_orden: identificador de orden de softmant
    """
    if id_orden is not None:
        sql = """ 
        SELECT M.Id_OTManttoMaster, M.Id_Elemento, M.Id_OTManttoElementos FROM Mov_OTMantto1ElementosHist M WHERE M.Id_OTManttoMaster = {} 
        """.format(id_orden)
        return funcion_ejecutar_softmant(sql)
    else:
        notificacion = Notificacion(
            titulo="El id de Orden de Servicio en softmant es un valor nulo",
            descripcion="Id de orden: {}, no fue encontrado en la base de integración, no se puede obtener detalles de orden".format(id_orden),
            aplicacion=modelsIncidente.notificacion.APP_SOFTMANT, nivel=modelsIncidente.notificacion.CRITICO)
        notificacion.save()
        return None


def obtener_materiales_orden(id_manto_elemento):
    """
    Retorna los materiales de una orden de softmant
    :param id_manto_elemento: identificador de elemento en manto de softmant
    """
    if id_manto_elemento is not None:
        sql = """ 
        SELECT R.Id_OTManttoElementos, R.Id_Material, R.Cantidad FROM Mov_OTMantto2DetalleRecursosHist R WHERE R.Id_TipoRecurso = 1 AND R.Id_OTManttoElementos = {} 
        """.format(id_manto_elemento)
        return funcion_ejecutar_softmant(sql)
    else:
        notificacion = Notificacion(
            titulo="El id de Orden de Servicio en softmant es un valor nulo",
            descripcion="Id de manto_elemento: {}, no fue encontrado en la base de integración, no se puede obtener materiales".format(id_manto_elemento),
            aplicacion=modelsIncidente.notificacion.APP_SOFTMANT, nivel=modelsIncidente.notificacion.CRITICO)
        notificacion.save()
        return None


def obtener_tareas_orden(id_manto_elemento):
    """
    Retorna lista de tareas de una orden de softmant
    :param id_manto_elemento: identificador de elemento en manto de softmant
    """
    if id_manto_elemento is not None:
        sql = """ 
        SELECT T.Id_OTManttoElementos, A.Descripcion, F.Falla, T.Id_Empleado, convert(nvarchar, T.Fecha_Realizada, 23) AS Fecha, T.TiempoRealizacion, T.Realizada
        FROM Mov_OTMantto3DetalleTareasHist T
        LEFT JOIN Det_TareasAsignadas A ON T.Id_TareaManto = A.Id_TareaManto
        LEFT JOIN Movi_ReporteDeFallas F ON T.Id_ReporteDeFallas = F.Id_ReporteDeFallas
        WHERE T.Id_OTManttoElementos = {} 
        """.format(id_manto_elemento)
        return funcion_ejecutar_softmant(sql)
    else:
        notificacion = Notificacion(
            titulo="El id de Orden de Servicio en softmant es un valor nulo",
            descripcion="Id de manto_elemento: {}, no fue encontrado en la base de integración, no se puede obtener tareas".format(id_manto_elemento),
            aplicacion=modelsIncidente.notificacion.APP_SOFTMANT, nivel=modelsIncidente.notificacion.CRITICO)
        notificacion.save()
        return None
