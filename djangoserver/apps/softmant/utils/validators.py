

def verificar_existe_empleado(id_empleado, lista):
    """
    Verifica si empleado existe en una lista
    :param id_empleado: identificador de empleado
    :param lista: lista de registros empleado de base de integracion
    :return: boolean
    """
    resultado = False
    for item in lista:
        if item.id_empleado == id_empleado:
            resultado = True
    return resultado


def verificar_existe_material(id_material, lista):
    """
    Verifica si material existe en lista
    :param id_material: identificador de material
    :param lista: lista de registros materiales
    :return: boolean
    """
    resultado = False
    for item in lista:
        if item.id_material == id_material:
            resultado = True
    return resultado


def verificar_existe_elemento(id_elemento, lista):
    """
    Verifica si elemento existe en lista
    :param id_elemento: identificador de elemento
    :param lista: lista de registros elementos
    :return: boolean
    """
    resultado = False
    for item in lista:
        if item.id_elemento == id_elemento:
            resultado = True
    return resultado


def verificar_existe_orden(id_orden, lista):
    """
    Verifica si orden existe en lista
    :param id_orden: identificador de orden
    :param lista: lista de registros orden de base de integracion
    :return: boolean
    """
    resultado = False
    for item in lista:
        if item.id_orden == id_orden:
            resultado = True
    return resultado
