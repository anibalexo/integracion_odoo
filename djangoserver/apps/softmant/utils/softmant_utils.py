from .softmant_querys import (
    obtener_empleados_softmant, obtener_materiales_softmant, obtener_elementos_softmant, obtener_ordenes_softmant,
    obtener_elementos_orden, obtener_materiales_orden, obtener_tareas_orden)
from .validators import verificar_existe_empleado, verificar_existe_material, verificar_existe_elemento, verificar_existe_orden
from ..models import Empleado, Material, Elemento, Orden, ElementoOrden, DetalleMaterial, DetalleTarea
from ...incidente import models as modelsIncidente
from ...incidente.models import Notificacion


def actualizar_empleados_sfm():
    """
    Actualiza empleados de softmant a base de integracion
    """
    lista_empleados = Empleado.objects.all()
    lista_empleados_sfm = obtener_empleados_softmant()
    if len(lista_empleados) > 0:
        for item in lista_empleados_sfm:
            if not verificar_existe_empleado(int(item["Id_Empleado"]), lista_empleados):
                empleado = Empleado(id_empleado=item["Id_Empleado"], dni=item["Clave"], nombres=item["Nombre"])
                empleado.save()
                print("Empleado creado: {}".format(empleado.nombres))
    else:
        for item in lista_empleados_sfm:
            empleado = Empleado(id_empleado=item["Id_Empleado"], dni=item["Clave"], nombres=item["Nombre"])
            empleado.save()
        print("Empleados creados inicialmente: {}".format(len(lista_empleados_sfm)))


def actualizar_materiales_sfm():
    """
    Actualiza materiales de softmant a base de integracion
    """
    lista_materiales = Material.objects.all()
    lista_materiales_sfm = obtener_materiales_softmant()
    if len(lista_materiales) > 0:
        for item in lista_materiales_sfm:
            if not verificar_existe_material(int(item["Id_Material"]), lista_materiales):
                material = Material(id_material=item["Id_Material"], unidad=item["Unidad"], codigo_material=item["Clave_Material"], descripcion=item["Descripcion"])
                material.save()
                print("Material creado: {}".format(material.descripcion))
    else:
        for item in lista_materiales_sfm:
            material = Material(id_material=item["Id_Material"], unidad=item["Unidad"], codigo_material=item["Clave_Material"], descripcion=item["Descripcion"])
            material.save()
        print("Materiales creados inicialmente: {}".format(len(lista_materiales_sfm)))


def actualizar_elementos_sfm():
    """
    Actualiza elementos de softmant a base de integracion
    """
    lista_elementos = Elemento.objects.all()
    lista_elementos_sfm = obtener_elementos_softmant()
    if len(lista_elementos) > 0:
        for item in lista_elementos_sfm:
            if not verificar_existe_elemento(int(item["Id_Elemento"]), lista_elementos):
                elemento = Elemento(id_elemento=item["Id_Elemento"], id_tipo_elemento=item["Id_TipoDeElemento"], nro_economico=item["NoEconomico"],
                                    descripcion=item["Descripcion"], codigo_barra=item["ElementoBarCode"])
                elemento.save()
                print("Elemento creado: {}".format(elemento.descripcion))
    else:
        for item in lista_elementos_sfm:
            elemento = Elemento(id_elemento=item["Id_Elemento"], id_tipo_elemento=item["Id_TipoDeElemento"], nro_economico=item["NoEconomico"],
                                descripcion=item["Descripcion"], codigo_barra=item["ElementoBarCode"])
            elemento.save()
        print("Elementos creados inicialmente: {}".format(len(lista_elementos_sfm)))


def obtener_empleado_orden(id_emp):
    """
    Verifica si empleado se encuentra registrado en base de integracion
    :param id_emp: identificador de empleado en softmant
    :return: instancia de empleado
    """
    try:
        emp = Empleado.objects.get(id_empleado=id_emp)
    except Empleado.DoesNotExist:
        emp = None
    return emp


def agregar_elementos_orden(orden, lista_elementos):
    """
    Agregar elementos a orden
    :param orden: instancia de orden de softmant
    :param lista_elementos: lista de elementos
    :return: instancias de elementos
    """
    for elem in lista_elementos:
        try:
            elemento = Elemento.objects.get(id_elemento=elem["Id_Elemento"])
        except Elemento.DoesNotExist:
            elemento = None
            notificacion = Notificacion(
                titulo="Elemento de Orden de Servicio no encontrado en base de integración",
                descripcion="El elemento con id: {}, no fue encontrado en la base de integración, orden de softmant: {}".format(elem["Id_Elemento"], orden.folio),
                aplicacion=modelsIncidente.notificacion.APP_SOFTMANT, nivel=modelsIncidente.notificacion.CRITICO)
            notificacion.save()
        if elemento is not None:
            elemento_orden = ElementoOrden(id_elemento=elem["Id_Elemento"], id_manto_elemento=elem["Id_OTManttoElementos"], orden=orden, elemento=elemento)
            elemento_orden.save()


def agregar_materiales_orden(elementoorden, lista_materiales):
    """
    Agregar materiales a elementos de orden
    :param elementoorden: instancia de elementoorden de softmant
    :param lista_materiales: lista de materiales
    """
    for mat in lista_materiales:
        try:
            material = Material.objects.get(id_material=mat["Id_Material"])
        except Material.DoesNotExist:
            material = None
            notificacion = Notificacion(
                titulo="Material de Orden de Servicio no encontrado en base de integración",
                descripcion="El material con id: {}, no fue encontrado en la base de integración, orden de softmant: {}".format(mat["Id_Elemento"], elementoorden.orden.folio),
                aplicacion=modelsIncidente.notificacion.APP_SOFTMANT, nivel=modelsIncidente.notificacion.CRITICO)
            notificacion.save()
        if material is not None:
            detallematerial_orden = DetalleMaterial(cantidad=mat["Cantidad"], material=material, elemento_orden=elementoorden)
            detallematerial_orden.save()


def agregar_detalletareas_orden(elementoorden, lista_tareas):
    """
    Agregar detalla de tareas a elementos de orden
    :param elementoorden: instancia de elementoorden de softmant
    :param lista_tareas: lista de detalletareas
    """
    for det in lista_tareas:
        desc = det["Descripcion"]
        if desc is None:
            desc = det["Falla"]
        detalletarea = DetalleTarea(descripcion=desc, id_empleado=det["Id_Empleado"], fecha=det["Fecha"], duracion=det["TiempoRealizacion"],
                                    realizada=det["Realizada"], elemento_orden=elementoorden)
        detalletarea.save()


def actualizar_ordenes_sfm():
    """
    Actualiza ordenes de softmant a base de integracion
    """
    lista_ordenes = Orden.objects.all()
    lista_ordenes_sfm = obtener_ordenes_softmant()
    if len(lista_ordenes) > 0:
        for item in lista_ordenes_sfm:
            if not verificar_existe_orden(int(item["Id_OTManttoMaster"]), lista_ordenes):
                empleado = obtener_empleado_orden(int(item["Id_Empleado"]))
                if empleado is None:
                    notificacion = Notificacion(
                        titulo="Empleado de Orden de Servicio no encontrado en base de integración",
                        descripcion="El empleado con id: {}, no fue encontrado en la base de integración, orden de softmant: {}".format(item["Id_Empleado"],
                                                                                                                                        item["Folio"]),
                        aplicacion=modelsIncidente.notificacion.APP_SOFTMANT, nivel=modelsIncidente.notificacion.CRITICO)
                    notificacion.save()
                if empleado is not None:
                    orden = Orden(id_orden=item["Id_OTManttoMaster"], empleado=empleado, estado=item["EstadoOS"], tipo=item["TipoOS"],
                                  folio=item["Folio"], descripcion=item["Descripcion"], fecha=item["Fecha"], comentarios=item["Comentarios"],
                                  duracion=item["TiempoRealizacion"], localizacion=item["Localizacion"], localizacion_descripcion=item["Localizacion_Descripcion"])
                    orden.save()
                    print("Orden creada: {}".format(orden.descripcion))
                    agregar_elementos_orden(orden, obtener_elementos_orden(orden.id_orden))
                    elementosorden = orden.elementoorden_orden.all()
                    for elo in elementosorden:
                        agregar_materiales_orden(elo, obtener_materiales_orden(elo.id_manto_elemento))
                        agregar_detalletareas_orden(elo, obtener_tareas_orden(elo.id_manto_elemento))

    else:
        for item in lista_ordenes_sfm:
            empleado = obtener_empleado_orden(int(item["Id_Empleado"]))
            if empleado is None:
                notificacion = Notificacion(
                    titulo="Empleado de Orden de Servicio no encontrado en base de integración",
                    descripcion="El empleado con id: {}, no fue encontrado en la base de integración, orden de softmant: {}".format(item["Id_Empleado"],
                                                                                                                                    item["Folio"]),
                    aplicacion=modelsIncidente.notificacion.APP_SOFTMANT, nivel=modelsIncidente.notificacion.CRITICO)
                notificacion.save()
            if empleado is not None:
                orden = Orden(id_orden=item["Id_OTManttoMaster"], empleado=empleado, estado=item["EstadoOS"], tipo=item["TipoOS"],
                              folio=item["Folio"], descripcion=item["Descripcion"], fecha=item["Fecha"], comentarios=item["Comentarios"],
                              duracion=item["TiempoRealizacion"], localizacion=item["Localizacion"], localizacion_descripcion=item["Localizacion_Descripcion"])
                orden.save()
                agregar_elementos_orden(orden, obtener_elementos_orden(orden.id_orden))
                elementosorden = orden.elementoorden_orden.all()
                for elo in elementosorden:
                    agregar_materiales_orden(elo, obtener_materiales_orden(elo.id_manto_elemento))
                    agregar_detalletareas_orden(elo, obtener_tareas_orden(elo.id_manto_elemento))
        print("Ordenes creadas inicialmente: {}".format(len(lista_ordenes_sfm)))


