from .detalle_material import DetalleMaterial
from .detalle_tarea import DetalleTarea
from .elemento import Elemento
from .elemento_orden import ElementoOrden
from .empleado import Empleado
from .material import Material
from .orden import Orden
