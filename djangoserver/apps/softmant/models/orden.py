from django.db import models

from .empleado import Empleado


class Orden(models.Model):
    """
    Modelo para integracion de ordenes de softmant a odoo
    """
    id_orden = models.IntegerField(default=0, help_text="Indentificador del orden en softmant")
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE, related_name='ordenes_empleado')
    estado = models.CharField(max_length=20)
    tipo = models.CharField(max_length=20)
    folio = models.CharField(max_length=20)
    descripcion = models.TextField()
    fecha = models.DateTimeField()
    comentarios = models.TextField(blank=True, null=True)
    duracion = models.FloatField(blank=True, null=True)
    localizacion = models.CharField(max_length=80, blank=True, null=True)
    localizacion_descripcion = models.CharField(max_length=80, blank=True, null=True)
    registro_odoo = models.BooleanField(default=False)
    registro_odoo_fecha = models.DateTimeField(blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'orden'
        verbose_name_plural = 'ordenes'

    def __str__(self):
        return self.folio
