from django.db import models

from .elemento_orden import ElementoOrden
from .material import Material


class DetalleMaterial(models.Model):
    """
    Modelo para integracion de materiales en orden de softmant
    """
    cantidad = models.IntegerField()
    material = models.ForeignKey(Material, on_delete=models.CASCADE, related_name='detallematerial_material')
    elemento_orden = models.ForeignKey(ElementoOrden, on_delete=models.CASCADE, related_name='detallematerial_elementoorden')
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'detalle de material'
        verbose_name_plural = 'detalle de materiales'

    def __str__(self):
        return self.material

