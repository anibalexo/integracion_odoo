from django.db import models

from .elemento_orden import ElementoOrden


class DetalleTarea(models.Model):
    """
    Modelo para integracion de detalle de tareas en orden de softmant
    """
    descripcion = models.TextField(blank=True, null=True)
    id_empleado = models.IntegerField()
    fecha = models.DateField(blank=True, null=True)
    duracion = models.FloatField(blank=True, null=True)
    realizada = models.BooleanField(default=False)
    elemento_orden = models.ForeignKey(ElementoOrden, on_delete=models.CASCADE, related_name='detalletarea_elementoorden')
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'detalle de tarea'
        verbose_name_plural = 'detalle de tareas'

    def __str__(self):
        return self.descripcion
