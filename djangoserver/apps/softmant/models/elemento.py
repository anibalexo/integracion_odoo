from django.db import models


class Elemento(models.Model):
    """
    Modelo para integracion de elementos de softmant a odoo
    """
    id_odoo = models.IntegerField(default=0, help_text="Indentificador de elemento en ERP Odoo")
    id_elemento = models.IntegerField(default=0, help_text="Indentificador del elemento en softflot")
    id_tipo_elemento = models.IntegerField(default=0)
    nro_economico = models.CharField(max_length=50, null=True, blank=True)
    descripcion = models.CharField(max_length=150)
    codigo_barra = models.CharField(max_length=200)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'elemento'
        verbose_name_plural = 'elementos'

    def __str__(self):
        return "({}) {}".format(self.id_elemento, self.descripcion)


