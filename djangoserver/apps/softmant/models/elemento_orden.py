from django.db import models

from .elemento import Elemento
from .orden import Orden


class ElementoOrden(models.Model):
    """
    Modelo para integracion de elemento de orden de softmant
    """
    id_elemento = models.IntegerField()
    id_manto_elemento = models.IntegerField()
    orden = models.ForeignKey(Orden, on_delete=models.CASCADE, related_name='elementoorden_orden')
    elemento = models.ForeignKey(Elemento, on_delete=models.CASCADE, related_name='elementoorden_elemento')
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'elemento de orden'
        verbose_name_plural = 'elementos de orden'

    def __str__(self):
        return "{} - {}".format(self.orden, self.elemento)
