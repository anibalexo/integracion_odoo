from celery import Celery
from celery.utils.log import get_task_logger

from .utils.softflot_utils import actualizar_empleados, actualizar_vehiculos, actualizar_materiales, actualizar_ordenes

app = Celery()

_logger = get_task_logger(__name__)


@app.task
def sincronizar_ordenes_softflot():
    """
    Metodo para obtener ordenes de servicio
    desde softflot hacia base de integracion
    :return: True
    """
    _logger.info("Sincronizando ordenes de softflot...")
    try:
        actualizar_empleados()
        actualizar_vehiculos()
        actualizar_materiales()
        actualizar_ordenes()
        resp = True
    except Exception as e:
        resp = e
    return resp

