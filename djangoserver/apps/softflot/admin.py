from django.contrib import admin

from .models import Vehiculo, Material, Empleado, Orden, DetalleOrden, DetalleMaterial


class VehiculoAdmin(admin.ModelAdmin):
    """
    Model admin para vehiculo
    """
    list_display = ('pk', 'id_odoo', 'id_vehiculo', 'codigo_barras', 'descripcion', 'nro_economico', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('codigo_barras', 'nro_economico')
    list_per_page = 10
    ordering = ('-fecha_creacion',)


class MaterialAdmin(admin.ModelAdmin):
    """
    Model admin para materiales
    """
    list_display = ('pk', 'id_odoo', 'id_material', 'unidad', 'id_unidad_odoo', 'codigo_material', 'descripcion', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('codigo_material', 'descripcion')
    list_per_page = 10
    ordering = ('-fecha_creacion',)


class EmpleadoAdmin(admin.ModelAdmin):
    """
    Model admin para empleados
    """
    list_display = ('pk', 'id_odoo', 'id_empleado', 'dni', 'nombres', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('dni', 'nombres')
    list_per_page = 15
    ordering = ('-fecha_creacion',)


class OrdenAdmin(admin.ModelAdmin):
    """
    Model admin para ordenes de servicio
    """
    list_display = ('pk', 'id_orden', 'empleado', 'vehiculo', 'tipo', 'folio', 'descripcion', 'fecha', 'duracion', 'registro_odoo', 'registro_odoo_fecha')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('empleado__nombres', 'vehiculo__nro_economico', 'folio', 'descripcion')
    list_filter = ('registro_odoo', 'tipo')
    list_per_page = 15
    ordering = ('-fecha_creacion',)


class DetalleOrdenAdmin(admin.ModelAdmin):
    """
    Model admin para detalles de ordenes de servicio
    """
    list_display = ('pk', 'fecha', 'id_empleado', 'descripcion', 'duracion', 'realizada', 'orden', 'fecha_creacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('descripcion', 'orden__descripcion')
    list_filter = ('realizada',)
    list_per_page = 15
    ordering = ('-fecha_creacion',)


class DetalleMaterialAdmin(admin.ModelAdmin):
    """
    Model admin para detalles de materiales de ordenes de servicio
    """
    list_display = ('pk', 'cantidad', 'orden', 'material', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('material__descripcion', 'orden__descripcion', 'orden__folio')
    list_per_page = 15
    ordering = ('-fecha_creacion',)


admin.site.register(Vehiculo, VehiculoAdmin)
admin.site.register(Material, MaterialAdmin)
admin.site.register(Empleado, EmpleadoAdmin)
admin.site.register(Orden, OrdenAdmin)
admin.site.register(DetalleOrden, DetalleOrdenAdmin)
admin.site.register(DetalleMaterial, DetalleMaterialAdmin)