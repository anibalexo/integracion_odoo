from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import InicioSoftflotTemplateView, VehiculoListView, EmpleadoListView, MaterialListView, OrdenListView, DetalleOrdenDetailView

urlpatterns = [
    path('', login_required(InicioSoftflotTemplateView.as_view()), name='softflot-inicio'),
    path('vehiculo/', login_required(VehiculoListView.as_view()), name='softflot-vehiculo'),
    path('empleado/', login_required(EmpleadoListView.as_view()), name='softflot-empleado'),
    path('material/', login_required(MaterialListView.as_view()), name='softflot-material'),
    path('orden/', login_required(OrdenListView.as_view()), name='softflot-orden'),
    path('orden/<int:pk>', login_required(DetalleOrdenDetailView.as_view()), name='softflot-orden-detalle'),
]
