# Generated by Django 2.2.14 on 2021-10-26 12:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Empleado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_odoo', models.IntegerField(default=0, help_text='Indentificador del empleado en ERP Odoo')),
                ('id_empleado', models.IntegerField(default=0, help_text='Indentificador del empleado en softflot')),
                ('dni', models.CharField(max_length=11, unique=True)),
                ('nombres', models.CharField(max_length=80)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'empleado',
                'verbose_name_plural': 'empleados',
            },
        ),
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_odoo', models.IntegerField(default=0, help_text='Indentificador del material (producto) en ERP Odoo')),
                ('id_material', models.IntegerField(default=0, help_text='Indentificador del material (producto) en Softflot')),
                ('unidad', models.CharField(max_length=20)),
                ('codigo_material', models.CharField(max_length=20)),
                ('descripcion', models.CharField(max_length=100)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'material',
                'verbose_name_plural': 'materiales',
            },
        ),
        migrations.CreateModel(
            name='Vehiculo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_odoo', models.IntegerField(default=0, help_text='Indentificador del vehiculo en ERP Odoo')),
                ('id_vehiculo', models.IntegerField(default=0, help_text='Indentificador del vehiculo en Softflot')),
                ('codigo_barras', models.CharField(max_length=20)),
                ('nro_economico', models.CharField(max_length=20)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'vehiculo',
                'verbose_name_plural': 'vehiculos',
            },
        ),
        migrations.CreateModel(
            name='Orden',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_orden', models.IntegerField(default=0, help_text='Indentificador del orden en softflot')),
                ('estado', models.CharField(max_length=20)),
                ('tipo', models.CharField(max_length=20)),
                ('folio', models.CharField(max_length=20)),
                ('descripcion', models.CharField(max_length=150)),
                ('fecha', models.DateTimeField()),
                ('comentarios', models.CharField(blank=True, max_length=150, null=True)),
                ('duracion', models.DecimalField(decimal_places=2, default=0.0, max_digits=4)),
                ('registro_odoo', models.BooleanField(default=False)),
                ('registro_odoo_fecha', models.DateTimeField(blank=True, null=True)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
                ('empleado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ordenes_empleado', to='softflot.Empleado')),
                ('vehiculo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ordenes_vehiculo', to='softflot.Vehiculo')),
            ],
            options={
                'verbose_name': 'orden',
                'verbose_name_plural': 'ordenes',
            },
        ),
        migrations.CreateModel(
            name='DetalleOrden',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(blank=True, null=True)),
                ('id_empleado', models.IntegerField(blank=True, null=True)),
                ('descripcion', models.CharField(max_length=200)),
                ('duracion', models.DecimalField(decimal_places=2, default=0.0, max_digits=4)),
                ('realizada', models.BooleanField(default=False)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
                ('orden', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='detalleorden_orden', to='softflot.Orden')),
            ],
            options={
                'verbose_name': 'detalle de orden',
                'verbose_name_plural': 'detalle de ordenes',
            },
        ),
        migrations.CreateModel(
            name='DetalleMaterial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad', models.IntegerField()),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
                ('material', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='detallematerial_material', to='softflot.Material')),
                ('orden', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='detallematerial_orden', to='softflot.Orden')),
            ],
            options={
                'verbose_name': 'detalle de material',
                'verbose_name_plural': 'detalle de materiales',
            },
        ),
    ]
