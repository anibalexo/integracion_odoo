from django.db import models


class Empleado(models.Model):
    """
    Modelo para integracion de empleados de odoo y softflot
    """
    id_odoo = models.IntegerField(default=0, help_text="Indentificador del empleado en ERP Odoo")
    id_empleado = models.IntegerField(default=0, help_text="Indentificador del empleado en softflot")
    dni = models.CharField(max_length=11, unique=True)
    nombres = models.CharField(max_length=80)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'empleado'
        verbose_name_plural = 'empleados'

    def __str__(self):
        return self.nombres
