from django.db import models

from .orden import Orden


class DetalleOrden(models.Model):
    """
    Modelo para integracion de detalle de ordenes de softflot a odoo
    """
    fecha = models.DateField(blank=True, null=True)
    id_empleado = models.IntegerField(blank=True, null=True)
    descripcion = models.CharField(max_length=200, blank=True, null=True)
    duracion = models.FloatField(blank=True, null=True)
    realizada = models.BooleanField(default=False)
    orden = models.ForeignKey(Orden, on_delete=models.CASCADE, related_name='detalleorden_orden')
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'detalle de orden'
        verbose_name_plural = 'detalle de ordenes'

    def __str__(self):
        return self.descripcion
