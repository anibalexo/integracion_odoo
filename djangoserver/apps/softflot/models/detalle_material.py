from django.db import models

from .material import Material
from .orden import Orden


class DetalleMaterial(models.Model):
    """
    Modelo para integracion de detalle materiales en ordenes de softflot a odoo
    """
    cantidad = models.IntegerField()
    orden = models.ForeignKey(Orden, on_delete=models.CASCADE, related_name='detallematerial_orden')
    material = models.ForeignKey(Material, on_delete=models.CASCADE, related_name='detallematerial_material')
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'detalle de material'
        verbose_name_plural = 'detalle de materiales'

    def __str__(self):
        return "{} - {}".format(self.orden, self.material)
