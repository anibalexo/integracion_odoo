from django.db import models


class Material(models.Model):
    """
    Modelo para integracion de materiales de odoo y softflot
    """
    id_odoo = models.IntegerField(default=0, help_text="Indentificador del material (producto) en ERP Odoo")
    id_material = models.IntegerField(default=0, help_text="Indentificador del material (producto) en Softflot")
    unidad = models.CharField(max_length=20)
    id_unidad_odoo = models.IntegerField(default=0, help_text="Indentificador del tipo unidad en ERP Odoo")
    codigo_material = models.CharField(max_length=20)
    descripcion = models.CharField(max_length=100)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'material'
        verbose_name_plural = 'materiales'

    def __str__(self):
        return self.codigo_material
