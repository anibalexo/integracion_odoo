from django.db import models


class Vehiculo(models.Model):
    """
    Modelo para integracion de vehiculos de odoo y softflot
    """
    id_odoo = models.IntegerField(default=0, help_text="Indentificador del vehiculo en ERP Odoo")
    id_vehiculo = models.IntegerField(default=0, help_text="Indentificador del vehiculo en Softflot")
    codigo_barras = models.CharField(max_length=20)
    descripcion = models.TextField()
    nro_economico = models.CharField(max_length=80)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'vehiculo'
        verbose_name_plural = 'vehiculos'

    def __str__(self):
        return self.nro_economico
