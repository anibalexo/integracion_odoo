from django.views.generic import TemplateView, ListView, DetailView

from .models import Vehiculo, Empleado, Material, Orden


def obtener_registros_no_registrados(lista):
    resultado = 0
    for item in lista:
        if item.id_odoo == 0:
            resultado += 1
    return resultado


def obtener_ordenes_estado(lista):
    subidas = 0
    pendientes = 0
    for item in lista:
        if item.registro_odoo == True:
            subidas += 1
        else:
            pendientes += 1
    return {"subidas": subidas, "pendientes": pendientes}


class InicioSoftflotTemplateView(TemplateView):
    template_name = 'softflot/inicio.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        empleados = Empleado.objects.all()
        context["num_empleados"] = len(empleados)
        context["num_empleados_odoo"] = obtener_registros_no_registrados(empleados)
        vehiculos = Vehiculo.objects.all()
        context["num_vehiculos"] = len(vehiculos)
        context["num_vehiculos_odoo"] = obtener_registros_no_registrados(vehiculos)
        materiales = Material.objects.all()
        context["num_materiales"] = len(materiales)
        context["num_materiales_odoo"] = obtener_registros_no_registrados(materiales)
        ordenes = Orden.objects.all()
        context["num_ordenes"] = len(ordenes)
        context["num_ordenes_estado"] = obtener_ordenes_estado(ordenes)
        return context


class VehiculoListView(ListView):
    model = Vehiculo
    ordering = ('-fecha_creacion',)
    paginate_by = 15
    template_name = 'softflot/vehiculo_list.html'


class EmpleadoListView(ListView):
    model = Empleado
    ordering = ('-fecha_creacion',)
    paginate_by = 15
    template_name = 'softflot/empleado_list.html'


class MaterialListView(ListView):
    model = Material
    ordering = ('-fecha_creacion',)
    paginate_by = 15
    template_name = 'softflot/material_list.html'


class OrdenListView(ListView):
    model = Orden
    ordering = ('-fecha_creacion',)
    paginate_by = 15
    template_name = 'softflot/orden_list.html'


class DetalleOrdenDetailView(DetailView):
    model = Orden
    template_name = 'softflot/orden_detalle.html'

