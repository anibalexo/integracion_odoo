
from django.db import connections

from ...incidente import models as modelsIncidente
from ...incidente.models import Notificacion


def dictfetchall(cursor):
    "Retorna filas de cursor como diccionario"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def funcion_ejecutar(sql):
    """
    Retorna diccionario obtenido al ejecutar un sql
    :param sql: sentencia sql para ejecutar
    :return: diccionario
    """
    cursor = connections['softflot_db'].cursor()
    try:
        cursor.execute(sql)
        return dictfetchall(cursor)
    except Exception as e:
        cursor.close()
        return False, e
    finally:
        cursor.close()


def obtener_empleados():
    """Retorna lista de empleados"""
    sql = """
    SELECT E.Id_Empleado, E.Clave, E.Nombre FROM Cat_Empleados E ORDER BY E.Nombre
    """
    return funcion_ejecutar(sql)


def obtener_materiales():
    """Retorna lista materiales"""
    sql = """
    SELECT M.Id_Material, U.Descripcion AS Unidad, M.Clave_Material, M.Descripcion FROM Cat_Materiales M INNER JOIN Cat_Unidades U ON M.Id_Unidad = U.Id_Unidad 
    ORDER BY M.Id_Unidad
    """
    return funcion_ejecutar(sql)


def obtener_vehiculos():
    """Retorna lista de vehiculos"""
    sql = """
    SELECT V.Id_Vehiculo, V.CodigoDeBarras, V.Concatena, V.NoEconomico FROM Mast_Vehiculos V ORDER BY V.Id_Vehiculo 
    """
    return funcion_ejecutar(sql)


def obtener_ordenes():
    """Retorna lista de ordenes cerradas en softflot mayores al 2021-10-01"""
    sql = """
    SELECT M.Id_OSMaster, M.Id_Empleado, S.Descripcion AS EstadoOS, M.Id_Vehiculo, T.Descripcion AS TipoOS, M.Folio, M.Descripcion,
    convert(nvarchar, M.Fecha, 20) AS Fecha, M.Comentarios, M.HorasDuracion FROM Movi_OSMasterHist M
    LEFT JOIN Sis_StatusOrdenServManto S ON M.Id_StausOrdenServManto = S.Id_StausOrdenServManto
    LEFT JOIN Sis_TipoDeOS T ON M.Id_TipoDeOS =  T.Id_TipoDeOS
    LEFT JOIN Cat_Empleados E ON M.Id_Empleado = E.Id_Empleado
    WHERE M.Id_StausOrdenServManto = 10 AND M.Fecha >= '2021-10-01' ORDER BY M.Folio
    """
    return funcion_ejecutar(sql)


def obtener_detalle_orden(id_orden):
    """
    Retorna el detalle de tareas realizadas en orden
    :param id_orden: identificador de orden de softflot
    """
    if id_orden is not None:
        sql = """
        SELECT D.Id_OSMaster, T.Descripcion, F.Falla, D.Id_Vehiculo, D.Id_Empleado, convert(nvarchar, D.FechaRealizada, 23) AS FechaRealizada, D.HorasDuracion, 
        D.Realizada FROM Movi_OSDetalleHist D 
        LEFT JOIN TareasManto T ON D.Id_TareaManto = T.Id_TareaManto 
        LEFT JOIN Movi_ReporteDeFallas F ON D.Id_OSMaster = F.Id_OSMaster 
        WHERE D.Id_OSMaster = {} 
        """.format(id_orden)
        return funcion_ejecutar(sql)
    else:
        notificacion = Notificacion(
            titulo="El id de Orden de Servicio en softflot es un valor nulo",
            descripcion="Id de orden: {}, no fue encontrado en la base de integración, no se puede obtener detalles de orden".format(id_orden),
            aplicacion=modelsIncidente.notificacion.APP_SOFTFLOT, nivel=modelsIncidente.notificacion.CRITICO)
        notificacion.save()
        return None


def obtener_materiales_orden(id_orden):
    """
    Retorna materiales utilizados en orden
    :param id_orden: identificador de orden de softflot
    :return: json
    """
    if id_orden is not None:
        sql = """
        SELECT C.Id_OSMaster, C.Id_Material, C.Cantidad FROM Movi_OSConsumos C WHERE C.Id_TipoRecurso = 1 AND C.Id_OSMaster = {} 
        """.format(id_orden)
        return funcion_ejecutar(sql)
    else:
        notificacion = Notificacion(
            titulo="El id de Orden de Servicio en softflot es un valor nulo",
            descripcion="Id de orden: {}, no fue encontrado en la base de integración, no se puede obtener detalles de materiales".format(id_orden),
            aplicacion=modelsIncidente.notificacion.APP_SOFTFLOT, nivel=modelsIncidente.notificacion.CRITICO)
        notificacion.save()
        return False
