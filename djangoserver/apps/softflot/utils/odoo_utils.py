import logging

from ..models import Orden, Empleado

_logger = logging.getLogger(__name__)


def genera_lista_actividades(orden):
    """
    Genera lista de actividades de una orden de softflot
    :param orden: instancia de modelo orden
    :return: list
    """
    _lista = []
    for act in orden.detalleorden_orden.filter(realizada=True):
        if act.id_empleado is None:
            id_emp = orden.empleado.id_empleado
        else:
            id_emp = act.id_empleado

        if act.fecha is None:
            fecha = act.fecha.strftime("%Y-%m-%d")
        else:
            fecha = orden.fecha.strftime("%Y-%m-%d")

        try:
            empleado = Empleado.objects.get(id_empleado=id_emp)
        except Exception as e:
            _logger.error("No se encontro el empleado con id: {}, descripcion: {}".format(id_emp, e))

        item = {
            'date': fecha,
            'employee_id': empleado.id_odoo,
            'name': act.descripcion,
            'unit_amount': act.duracion
        }
        _lista.append(item)
    return _lista


def genera_lista_materiales(orden):
    """
    Genera lista de materiales de una orden de softflot
    :param orden: instancia de modelo orden
    :return: list
    """
    _lista = []
    for mtr in orden.detallematerial_orden.all():
        item = {
            'product_id': mtr.material.id_odoo,
            'product_uom_qty': mtr.cantidad,
            'product_uom': mtr.material.id_unidad_odoo
        }
        _lista.append(item)
    return _lista


def obtener_tipo_orden(orden):
    """
    Retorna string con tipo de orden
    :param orden: instancia de modelo orden
    :return: string
    """
    _tipo = ""
    if orden is not None:
        if orden.tipo == "OSP":
            _tipo = "preventive"
        elif orden.tipo == "OSC":
            _tipo = "corrective"
        else:
            _tipo = "other"
    return _tipo


def recalcular_duracion_total(orden):
    """
    Retorna la duracion solo con tareas realizadas
    :param orden: instancia de modelo orden
    :return: float
    """
    _duracion = 0
    for act in orden.detalleorden_orden.filter(realizada=True):
        _duracion += act.duracion
    return round(_duracion, 2)


def obtener_dict_ordenes_softflot():
    """
    Genera diccionario de orden para subir a odoo
    :return: json
    """
    lista_ordenes = Orden.objects.filter(registro_odoo=False, registro_odoo_fecha=None)
    _lista_dict = []
    for ord in lista_ordenes:
        item = {
            "name": ord.folio,
            "employee_id": ord.empleado.id_odoo,
            "equipment_id": 1,
            "maintenance_type": obtener_tipo_orden(ord),
            "maintenance_team_id": 1,
            "schedule_date": ord.fecha.strftime("%Y-%m-%d %H:%M:%S"),
            "duration": recalcular_duracion_total(ord),
            "email_cc": "ejemplo@lojagas.com",
            "company_id": 1,
            "description": ord.descripcion,
            "workings_hours": genera_lista_actividades(ord),
            "product_list": genera_lista_materiales(ord)
        }
        _lista_dict.append(item)
    return _lista_dict
