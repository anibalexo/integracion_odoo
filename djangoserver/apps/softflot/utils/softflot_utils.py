import logging

from .softflot_querys import obtener_vehiculos, obtener_materiales, obtener_empleados, obtener_ordenes, obtener_detalle_orden, obtener_materiales_orden
from .validators import verificar_existe_vehiculo, verificar_existe_material, verificar_existe_empleado, verificar_existe_orden
from ..models import Vehiculo, Material, Empleado, DetalleOrden, DetalleMaterial, Orden
from ...incidente import models as modelsIncidente
from ...incidente.models import Notificacion

_logger = logging.getLogger(__name__)


def actualizar_vehiculos():
    """
    Actualiza vehiculos de softflot a base de integracion
    """
    lista_vehiculos = Vehiculo.objects.all()
    lista_vehiculos_sf = obtener_vehiculos()
    if len(lista_vehiculos) > 0:
        for item in lista_vehiculos_sf:
            if not verificar_existe_vehiculo(int(item["Id_Vehiculo"]), lista_vehiculos):
                vehiculo = Vehiculo(id_vehiculo=item["Id_Vehiculo"], codigo_barras=item["CodigoDeBarras"], descripcion=item["Concatena"],
                                    nro_economico=item["NoEconomico"])
                vehiculo.save()
                _logger.info("Vehiculo creado: {}".format(vehiculo.descripcion))
    else:
        for item in lista_vehiculos_sf:
            vehiculo = Vehiculo(id_vehiculo=item["Id_Vehiculo"], codigo_barras=item["CodigoDeBarras"], descripcion=item["Concatena"],
                                nro_economico=item["NoEconomico"])
            vehiculo.save()
        _logger.info("Vehiculos creados incialmente: {}".format(len(lista_vehiculos_sf)))


def actualizar_materiales():
    """
    Actualiza materiales de sotflot a base de integracion
    """
    lista_materiales = Material.objects.all()
    lista_materiales_sf = obtener_materiales()
    if len(lista_materiales) > 0:
        for item in lista_materiales_sf:
            if not verificar_existe_material(int(item["Id_Material"]), lista_materiales):
                material = Material(id_material=item["Id_Material"], unidad=item["Unidad"], codigo_material=item["Clave_Material"], descripcion=item["Descripcion"])
                material.save()
                _logger.info("Material creado: {}".format(material.descripcion))
    else:
        for item in lista_materiales_sf:
            material = Material(id_material=item["Id_Material"], unidad=item["Unidad"], codigo_material=item["Clave_Material"], descripcion=item["Descripcion"])
            material.save()
        _logger.info("Materiales creados inicialmente: {}".format(len(lista_materiales_sf)))


def actualizar_empleados():
    """
    Actualiza empleados de softflot a base de integracion
    """
    lista_empleados = Empleado.objects.all()
    lista_empleados_sf = obtener_empleados()
    if len(lista_empleados) > 0:
        for item in lista_empleados_sf:
            if not verificar_existe_empleado(int(item["Id_Empleado"]), lista_empleados):
                empleado = Empleado(id_empleado=item["Id_Empleado"], dni=item["Clave"], nombres=item["Nombre"])
                empleado.save()
                _logger.info("Empleado creado: {}".format(empleado.descripcion))
    else:
        for item in lista_empleados_sf:
            empleado = Empleado(id_empleado=item["Id_Empleado"], dni=item["Clave"], nombres=item["Nombre"])
            empleado.save()
        _logger.info("Empleados creados inicialmente: {}".format(len(lista_empleados_sf)))


def obtener_empleado_orden(id_emp):
    """
    Verifica si empleado se encuentra registrado en base de integracion
    :param id_emp: identificador de empleado en softflot
    :return: instancia de empleado
    """
    try:
        emp = Empleado.objects.get(id_empleado=id_emp)
    except Empleado.DoesNotExist:
        emp = None
        _logger.error("Empleado no encontrado con id: {}".format(id_emp))
    return emp


def obtener_vehiculo_orden(id_veh):
    """
    Verifica si vehiculo se encuentra registrado en base de integracion
    :param id_veh: identificador de vehiculo en softflot
    :return: instancia de vehiculo
    """
    try:
        vehi = Vehiculo.objects.get(id_vehiculo=id_veh)
    except Vehiculo.DoesNotExist:
        vehi = None
        _logger.error("Vehiculo no encontrado con id: {}".format(id_veh))
    return vehi


def agregar_detalles_orden(orden, lista_detalles_orden):
    """
    Agrega detalle de ordenes a orden
    :param orden: instancia de orden
    :param lista_detalles_orden: detalles de orden de softflot
    """
    for det in lista_detalles_orden:
        desc = det["Descripcion"]
        if orden.tipo == 'OSC':
            desc = det["Falla"]
        detalle_orden = DetalleOrden(fecha=det["FechaRealizada"], id_empleado=det["Id_Empleado"], descripcion=desc,
                                     duracion=det["HorasDuracion"], realizada=det["Realizada"], orden=orden)
        detalle_orden.save()


def agregar_materiales_orden(orden, lista_materiales_orden):
    """
    Agrega detalle de materiales a orden
    :param orden: instancia de orden
    :param lista_materiales_orden: lista de materiales
    """
    for det in lista_materiales_orden:
        try:
            material = Material.objects.get(id_material=det["Id_Material"])
        except Material.DoesNotExist:
            material = None
            notificacion = Notificacion(
                titulo="Material de Orden de Servicio no encontrado en base de integración",
                descripcion="El material con id: {}, no fue encontrado en la base de integración, orden de softflot: {}".format(det["Id_Material"], orden.folio),
                aplicacion=modelsIncidente.notificacion.APP_SOFTFLOT, nivel=modelsIncidente.notificacion.CRITICO)
            notificacion.save()
        if material is not None:
            detalle_material = DetalleMaterial(cantidad=det["Cantidad"], material=material, orden=orden)
            detalle_material.save()


def actualizar_ordenes():
    """
    Actualiza ordenes de softflot a base de integracion
    """
    lista_ordenes = Orden.objects.all()
    lista_ordenes_sf = obtener_ordenes()
    if len(lista_ordenes) > 0:
        for item in lista_ordenes_sf:
            if not verificar_existe_orden(int(item["Id_OSMaster"]), lista_ordenes):
                empleado = obtener_empleado_orden(int(item["Id_Empleado"]))
                vehiculo = obtener_vehiculo_orden(int(item["Id_Vehiculo"]))
                if empleado is None:
                    notificacion = Notificacion(
                        titulo="Empleado de Orden de Servicio no encontrado en base de integración",
                        descripcion="El empleado con id: {}, no fue encontrado en la base de integración, orden de softflot: {}".format(item["Id_Empleado"],
                                                                                                                                        item["Folio"]),
                        aplicacion=modelsIncidente.notificacion.APP_SOFTFLOT, nivel=modelsIncidente.notificacion.CRITICO)
                    notificacion.save()

                if vehiculo is None:
                    notificacion = Notificacion(
                        titulo="Vehiculo de Orden de Servicio no encontrado en base de integración",
                        descripcion="El vehiculo con id: {}, no fue encontrado en la base de integración, orden de softflot: {}".format(item["Id_Vehiculo"],
                                                                                                                                        item["Folio"]),
                        aplicacion=modelsIncidente.notificacion.APP_SOFTFLOT, nivel=modelsIncidente.notificacion.CRITICO)
                    notificacion.save()

                if empleado is not None and vehiculo is not None:
                    orden = Orden(id_orden=item["Id_OSMaster"], empleado=empleado, estado=item["EstadoOS"], vehiculo=vehiculo, tipo=item["TipoOS"],
                                  folio=item["Folio"], descripcion=item["Descripcion"], fecha=item["Fecha"], comentarios=item["Comentarios"],
                                  duracion=item["HorasDuracion"])
                    orden.save()
                    _logger.info("Orden creada: {}".format(orden.descripcion))
                    agregar_detalles_orden(orden, obtener_detalle_orden(orden.id_orden))
                    agregar_materiales_orden(orden, obtener_materiales_orden(orden.id_orden))
    else:
        for item in lista_ordenes_sf:
            empleado = obtener_empleado_orden(int(item["Id_Empleado"]))
            vehiculo = obtener_vehiculo_orden(int(item["Id_Vehiculo"]))
            if empleado is None:
                notificacion = Notificacion(
                    titulo="Empleado de Orden de Servicio no encontrado en base de integración",
                    descripcion="El empleado con id: {}, no fue encontrado en la base de integración, orden de softflot: {}".format(item["Id_Empleado"],
                                                                                                                                    item["Folio"]),
                    aplicacion=modelsIncidente.notificacion.APP_SOFTFLOT, nivel=modelsIncidente.notificacion.CRITICO)
                notificacion.save()

            if vehiculo is None:
                notificacion = Notificacion(
                    titulo="Vehiculo de Orden de Servicio no encontrado en base de integración",
                    descripcion="El vehiculo con id: {}, no fue encontrado en la base de integración, orden de softflot: {}".format(item["Id_Vehiculo"],
                                                                                                                                    item["Folio"]),
                    aplicacion=modelsIncidente.notificacion.APP_SOFTFLOT, nivel=modelsIncidente.notificacion.CRITICO)
                notificacion.save()

            if empleado is not None and vehiculo is not None:
                orden = Orden(id_orden=item["Id_OSMaster"], empleado=empleado, estado=item["EstadoOS"], vehiculo=vehiculo, tipo=item["TipoOS"], folio=item["Folio"],
                              descripcion=item["Descripcion"], fecha=item["Fecha"], comentarios=item["Comentarios"], duracion=item["HorasDuracion"])
                orden.save()
                agregar_detalles_orden(orden, obtener_detalle_orden(orden.id_orden))
                agregar_materiales_orden(orden, obtener_materiales_orden(orden.id_orden))
        _logger.info("Ordenes creadas inicialmente: {}".format(len(lista_ordenes_sf)))


