from django.contrib import admin
from .models import Notificacion


class NotificacionAdmin(admin.ModelAdmin):
    """
    Model admin para incidente
    """
    list_display = ('pk', 'titulo', 'aplicacion', 'nivel', 'resuelto', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('titulo', 'aplicacion')
    list_per_page = 10
    ordering = ('-fecha_creacion',)


admin.site.register(Notificacion, NotificacionAdmin)