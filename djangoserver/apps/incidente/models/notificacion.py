from django.db import models

APP_OTRO = "Otro"
APP_BIOMETRICO = "Biometrico"
APP_SOFTFLOT = "Softflot"
APP_SOFTMANT = "Softmant"

APPS = (
    (APP_OTRO, "Otro"),
    (APP_BIOMETRICO, "Biometrico"),
    (APP_SOFTFLOT, "Softflot"),
    (APP_SOFTMANT, "Softfmant"),
)

CRITICO = 0
MAYOR = 1
MENOR = 2
CONSULTA = 3

NIVELES = (
    (CRITICO, 'Critico'),
    (MAYOR, 'Mayor'),
    (MENOR, 'Menor'),
    (CONSULTA, 'Consulta'),
)


class Notificacion(models.Model):
    """
    Modelo para notificacion
    """
    titulo = models.CharField(max_length=200)
    descripcion = models.TextField()
    aplicacion = models.CharField(max_length=50, choices=APPS, default=APP_OTRO)
    nivel = models.IntegerField(choices=NIVELES, default=0)
    resuelto = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'notificacion'
        verbose_name_plural = 'notificaciones'

    def __str__(self):
        return self.titulo
