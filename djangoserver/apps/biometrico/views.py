
from django.views.generic import TemplateView, ListView

from .models.periodo import Periodo
from .models.empleado import Empleado
from .models.dispositivo import Dispositivo
from .models.marcacion import Marcacion


class InicioTemplateView(TemplateView):
    template_name = 'biometrico/inicio.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        num_empleados = Empleado.objects.all().count()
        context['num_empleados'] = num_empleados
        lista_dispositivos = Dispositivo.objects.filter(activo=True).order_by('pk')
        context['lista_dispositivos'] = lista_dispositivos
        return context


class PeriodoListView(ListView):
    model = Periodo
    ordering = ('-fecha_creacion',)
    paginate_by = 15
    template_name = 'biometrico/periodo_list.html'


class EmpleadoListView(ListView):
    model = Empleado
    ordering = ('-id_user',)
    paginate_by = 15
    template_name = 'biometrico/empleado_list.html'


class DispositivoListView(ListView):
    model = Dispositivo
    ordering = ('pk',)
    paginate_by = 15
    template_name = 'biometrico/dispositivo_list.html'


class MarcacionListView(ListView):
    model = Marcacion
    ordering = ('-fecha',)
    paginate_by = 15
    template_name = 'biometrico/marcacion_list.html'


