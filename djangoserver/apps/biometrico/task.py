import datetime

from celery import Celery
from celery.utils.log import get_task_logger
from django.core.mail import EmailMessage, BadHeaderError
from django.http import HttpResponse
from django.template.loader import render_to_string

from .models.dispositivo import Dispositivo
from .utils import sincronizar_empleados, sincronizar_marcaciones
from .zk_py.exception import ZKError
from ..incidente import models as modelsIncidente
from ..incidente.models import Notificacion
from ..usuario.models import User

app = Celery()


_logger = get_task_logger(__name__)


@app.task
def sample_task():
    return "hola es una prueba"


@app.task
def sincronizar_dispositivos():
    """
    Metodo para sincronizar marcaciones de dispositivos
    hacia base de integración Odoo
    :return:
    """
    dispositivos = Dispositivo.objects.filter(activo=True)
    registros = []
    for dsp in dispositivos:
        _logger.info("Iniciando sincronización de dispositivo {}".format(dsp.nombre))
        try:
            _logger.info("Sincronizando empleados...")
            sincronizar_empleados(dsp)
            _logger.info("Sincronizando marcaciones...")
            sincronizar_marcaciones(dsp)
            _logger.info("Finalizo sincronización de dispositivos")
            registros.append({'estado': 'ok', 'nombre': dsp.nombre, 'ip': dsp.ip_address, 'descripcion': 'Sincronización realizada con éxito'})
        except ZKError as e:
            registros.append({'estado': 'error', 'nombre': dsp.nombre, 'ip': dsp.ip_address, 'descripcion': str(e)})
    if len(registros) > 0:
        enviar_mail_sincronizar_dispositivos(registros)
    return True


def enviar_mail_sincronizar_dispositivos(lista_registros):
    """
    Tarea asíncrona que envía un email a los administradores
    con resultados al sincronizar dispositivos
    :return:
    """
    print("Enviando...")
    enviado = True
    columnas_template = ["Nombre Dispositivo", "Dirección IP", "Resultado"]
    fecha = datetime.datetime.now()
    context = {"lista_registros": lista_registros, "columnas_template": columnas_template, "fecha": fecha}
    usuarios = User.objects.filter(is_staff=True)
    emails = [usuario.email for usuario in usuarios]
    subject = "Resultados de Sincronización"
    html_content = render_to_string('emails/sincronizacion_dispositivos.html', context)
    email = EmailMessage(subject, html_content)
    email.content_subtype = "html"
    email.bcc = emails
    if not usuarios:
        Notificacion.objects.create(
            titulo="Falta configurar administradores", descripcion="No se ha configurado usuarios con el atributo is_staff valor True, para el envio de correos",
            aplicacion=modelsIncidente.notificacion.APP_BIOMETRICO, nivel=modelsIncidente.notificacion.MAYOR)
    try:
        email.send()
    except BadHeaderError:
        _logger.info("No se ha enviado el correo con errores en los dispositivos")
        return HttpResponse('Invalid header found.')
    return enviado
