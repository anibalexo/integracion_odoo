
import sys
from datetime import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _

from ..zk_py import ZK
from ..zk_py.exception import ZKError

sys.path.append("zk")


class Dispositivo(models.Model):
    """
    Dispositivos biometricos que almacenan marcaciones
    """
    nombre = models.CharField(max_length=60)
    ubicacion = models.CharField(max_length=60, help_text="Ubicación fisica de dispositivo")
    ip_address = models.CharField(max_length=15, help_text="Dirección ip de dispositivo")
    puerto = models.IntegerField(default=0, help_text="Puerto de escucha de dispositivo")
    activo = models.BooleanField(default=True)
    firmware = models.CharField(max_length=80, blank=True, null=True)
    device_name = models.CharField(max_length=80, blank=True, null=True)
    serial = models.CharField(max_length=80, blank=True, null=True)
    mac = models.CharField(max_length=80, blank=True, null=True)
    plataform = models.CharField(max_length=80, blank=True, null=True)
    numero_registros = models.IntegerField(default=0)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'dispositivo'
        verbose_name_plural = 'dispositivos'

    def __init__(self, *args, **kwargs):
        self.zkconn = None
        super(Dispositivo, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.nombre

    def zk_connect(self):
        "Establecer conexion a dispositivo biometrico"
        device = ZK(self.ip_address, port=self.puerto, timeout=5, password=0, force_udp=False, ommit_ping=False)
        conn = device.connect()
        if conn:
            device.disable_device()
            self.zkconn = device
        else:
            raise ZKError(_('can\'t connect to terminal: zk_connect'))

    def zk_disconnect(self):
        "Desconectarse de dispositivo biometrico"
        if not self.zkconn:
            raise ZKError(_('can\'t connect to terminal: zk_disconnect'))
        self.zkconn.enable_device()
        self.zkconn.disconnect()

    def zk_get_users(self):
        "Obtener usuarios de dispositivo biometrico"
        if not self.zkconn:
            raise ZKError(_('can\'t connect to terminal: zk_get_users'))
        return self.zkconn.get_users()

    def zk_get_attendances(self):
        "Obtener registros de marcaciones de dispositivo biometrico"
        if not self.zkconn:
            raise ZKError(_('can\'t connect to terminal: zk_get_attendances'))
        attendances = self.zkconn.get_attendance()
        return attendances

    def zk_clear_attendances(self):
        "Borra registros de marcaciones de dispositivo biometrico"
        if not self.zkconn:
            raise ZKError(_('can\'t connect to terminal: zk_clear_attendances'))
        self.zkconn.clear_attendance()

    def zk_restart(self):
        "Reinicia dispositivo biometrico"
        if not self.zkconn:
            raise ZKError(_('can\'t connect to terminal: zk_restart'))
        self.restart()

    def zk_sync_time(self):
        "Actualizar hora de dispositivo biometrico"
        if not self.zkconn:
            raise ZKError(_('can\'t connect to terminal: zk_sync_time'))
        self.set_time(datetime.now())

    def zk_update_device_info(self):
        "Obtiene información de dispositivo"
        if not self.zkconn:
            raise ZKError(_('can\'t connect to terminal: zk_update_device_info'))
        try:
            self.firmware = self.zkconn.get_firmware_version()
            self.device_name = self.zkconn.get_device_name()
            self.serial = self.zkconn.get_serialnumber()
            self.mac = self.zkconn.get_mac()
            self.plataform = self.zkconn.get_platform()
            self.save()
        except Exception as e:
            raise ZKError(_('can\'t connect to terminal: zk_update_device_info'))

    def get_ultimo_periodo(self):
        "Retorna ultimo periodo de dispositivo"
        return self.periodosdispositivo.latest('pk')


