from django.db import models

from .dispositivo import Dispositivo


class Periodo(models.Model):
    """
    Periodos para identificar lotes de registros
    extraidos de dispositivo biometrico
    """
    fecha_inicial = models.DateTimeField()
    fecha_final = models.DateTimeField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    dispositivo = models.ForeignKey(Dispositivo, on_delete=models.CASCADE, related_name="periodosdispositivo")

    class Meta:
        verbose_name = 'periodo'
        verbose_name_plural = 'periodos'

    def __str__(self):
        return "[{}] {}_{}".format(self.pk, self.fecha_inicial, self.fecha_final)

    @property
    def numero_marcaciones(self):
        """Retorna numero de marcaciones vinculadas a periodo"""
        return self.marcacionesPeriodo.count()

    def actualizar_fecha_final(self):
        """Actualiza fecha final en base a ultima marcacion"""
        if self.marcacionesPeriodo.count() > 0:
            self.fecha_final = self.marcacionesPeriodo.latest('fecha').fecha
            self.save()
