from django.db import models


class Empleado(models.Model):
    """
    Empleados en dispositivos biometricos
    """
    nombres = models.CharField(max_length=100)
    id_user = models.IntegerField(help_text="Indentificador del empleado en dispositivo")
    privilegio = models.CharField(max_length=20)
    activo = models.BooleanField(default=True)
    id_odoo = models.IntegerField(default=0, help_text="Indentificador de empleado en ERP Odoo")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'empleado'
        verbose_name_plural = 'empleados'

    def __str__(self):
        return self.nombres
