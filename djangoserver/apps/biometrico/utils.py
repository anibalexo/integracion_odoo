from django.db.models import Max
from zk import const

from .models import Empleado, Marcacion, Periodo


def get_privilegio(user):
    privilege = "User"
    if user.privilege == const.USER_ADMIN:
        privilege = 'Admin'
    return privilege


def sincronizar_empleados(dispositivo):
    """
    Actualizamos empleados de biometrico
    en base de integración
    :param dispositivo: instancia modelo dispositivo
    :return: None
    """
    dispositivo.zk_connect()
    usuarios = dispositivo.zk_get_users()
    dispositivo.zk_disconnect()
    empleados = Empleado.objects.all().values_list('id_user', flat=True)
    if len(empleados) > 0:
        for usr in usuarios:
            if int(usr.user_id) not in empleados:
                obj = Empleado(nombres=usr.name, id_user=usr.user_id, privilegio=get_privilegio(usr), activo=True, id_odoo=0)
                obj.save()
    else:
        for usr in usuarios:
            obj = Empleado(nombres=usr.name, id_user=usr.user_id, privilegio=get_privilegio(usr), activo=True, id_odoo=0)
            obj.save()


def sincronizar_marcaciones(dispositivo):
    """
    Obtenemos registros de marcacion desde dispositivo
    biometrico hacia base de integracion odoo
    :param dispositivo: instancia modelo dispositivo
    :return:
    """
    dispositivo.zk_connect()
    periodos = Periodo.objects.filter(dispositivo=dispositivo)
    attendances = dispositivo.zk_get_attendances()
    dispositivo.numero_registros = len(attendances)
    dispositivo.save()
    if len(periodos) > 0:
        if len(attendances) > 0:
            fecha_inicial = periodos.aggregate(Max('fecha_final'))
            periodo = Periodo(fecha_inicial=fecha_inicial['fecha_final__max'], fecha_final=attendances[-1].timestamp, dispositivo=dispositivo)
            periodo.save()
            for record in attendances:
                try:
                    empl = Empleado.objects.get(id_user=int(record.user_id))
                except Empleado.DoesNotExist as e:
                    print("Error al encontrar usuario: {}".format(e))
                    continue
                if record.timestamp > periodo.fecha_inicial:
                    Marcacion.objects.create(fecha=record.timestamp, tipo=record.status, periodo=periodo, dispositivo=dispositivo, empleado=empl)
            periodo.actualizar_fecha_final()
    else:
        if len(attendances) > 0:
            periodo = Periodo(fecha_inicial=attendances[0].timestamp, fecha_final=attendances[-1].timestamp, dispositivo=dispositivo)
            periodo.save()
            for record in attendances:
                try:
                    empl = Empleado.objects.get(id_user=int(record.user_id))
                except Empleado.DoesNotExist as e:
                    print("Error al encontrar usuario: {}".format(e))
                    continue
                Marcacion.objects.create(fecha=record.timestamp, tipo=record.status, periodo=periodo, dispositivo=dispositivo, empleado=empl)
            periodo.actualizar_fecha_final()
    #LA SIGUIENTE LINEA DEBE ACTIVARSE CON AUTORIZACION DE JEFE DE TICS, PORQUE BORRA REGISTROS DE DISPOSITIVO FISICO
    #dispositivo.zk_clear_attendances()
    dispositivo.zk_disconnect()


def actualizar_info_dispositivo(dispositivo):
    """
    Actualiza informacion de dispositivo
    :param dispositivo: instancia de dispositivo
    :return: None
    """
    dispositivo.zk_connect()
    dispositivo.zk_update_device_info()
    dispositivo.zk_disconnect()
