from django.contrib import admin


from .models import Dispositivo, Periodo, Empleado, Marcacion


class DispositivoAdmin(admin.ModelAdmin):
    """
    Model admin para dispositivos biometricos
    """
    list_display = ('nombre', 'pk', 'ubicacion', 'ip_address', 'puerto', 'activo', 'fecha_creacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('nombre', 'ip_address')
    list_filter = ('activo',)
    list_per_page = 10
    ordering = ('-fecha_creacion',)


class PeriodoAdmin(admin.ModelAdmin):
    """
    Model admin para periodos de marcaciones
    """
    list_display = ('pk', 'fecha_inicial', 'fecha_final', 'numero_marcaciones', 'dispositivo', 'fecha_creacion')
    readonly_fields = ('fecha_creacion',)
    list_filter = ('dispositivo',)
    list_per_page = 16
    ordering = ('-fecha_creacion',)


class EmpleadoAdmin(admin.ModelAdmin):
    """
    Model admin para empleados
    """
    list_display = ('nombres', 'pk', 'id_user', 'activo', 'id_odoo', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('nombres', 'id_user')
    list_filter = ('activo',)
    list_per_page = 15
    ordering = ('nombres',)


class MarcacionAdmin(admin.ModelAdmin):
    """
    Model admin para marcaciones
    """
    list_display = ('pk', 'fecha', 'tipo', 'periodo', 'dispositivo', 'empleado')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    search_fields = ('empleado__nombres',)
    list_filter = ('dispositivo',)
    list_per_page = 16
    ordering = ('-pk',)


admin.site.register(Dispositivo, DispositivoAdmin)
admin.site.register(Periodo, PeriodoAdmin)
admin.site.register(Empleado, EmpleadoAdmin)
admin.site.register(Marcacion, MarcacionAdmin)

