from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import InicioTemplateView, PeriodoListView, EmpleadoListView, DispositivoListView, MarcacionListView

urlpatterns = [
    path('', login_required(InicioTemplateView.as_view()), name='biometrico-inicio'),
    path('periodo/', login_required(PeriodoListView.as_view()), name='biometrico-periodo'),
    path('empleado/', login_required(EmpleadoListView.as_view()), name='biometrico-empleado'),
    path('dispositivo/', login_required(DispositivoListView.as_view()), name='biometrico-dispositivo'),
    path('marcacion/', login_required(MarcacionListView.as_view()), name='biometrico-marcacion'),

]
