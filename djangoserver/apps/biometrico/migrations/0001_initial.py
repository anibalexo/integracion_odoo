# Generated by Django 2.2.14 on 2021-08-30 09:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Dispositivo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=60)),
                ('ubicacion', models.CharField(help_text='Ubicación fisica de dispositivo', max_length=60)),
                ('ip_address', models.CharField(help_text='Dirección ip de dispositivo', max_length=15)),
                ('puerto', models.IntegerField(default=0, help_text='Puerto de escucha de dispositivo')),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'dispositivo',
                'verbose_name_plural': 'dispositivos',
            },
        ),
        migrations.CreateModel(
            name='Empleado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombres', models.CharField(max_length=100)),
                ('id_user', models.IntegerField(help_text='Indentificador del empleado en dispositivo')),
                ('privilegio', models.CharField(max_length=20)),
                ('activo', models.BooleanField(default=True)),
                ('id_odoo', models.IntegerField(default=0, help_text='Indentificador de empleado en ERP Odoo')),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'empleado',
                'verbose_name_plural': 'empleados',
            },
        ),
        migrations.CreateModel(
            name='Periodo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicial', models.DateTimeField()),
                ('fecha_final', models.DateTimeField()),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'periodo',
                'verbose_name_plural': 'periodos',
            },
        ),
        migrations.CreateModel(
            name='Marcacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateTimeField()),
                ('tipo', models.IntegerField()),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
                ('dispositivo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='biometrico.Dispositivo')),
                ('empleado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='biometrico.Empleado')),
                ('periodo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='biometrico.Periodo')),
            ],
            options={
                'verbose_name': 'marcacion',
                'verbose_name_plural': 'marcaciones',
            },
        ),
    ]
