# INTEGRACION ODOO

##### Ejecutar migraciones

    - docker-compose exec djangoserver python manage.py makemigrations
    - docker-compose exec djangoserver python manage.py migrate
    - docker-compose -f production.yml exec djangoserver python manage.py migrate

    - docker exec -it {id contendor} /bin/bash

##### Ejecutar collectstatics

    - docker-compose -f production.yml exec django_server python manage.py collectstatic

### AMBIENTE DE DESARROLLO
Para construir la imagen y ejecutar el proyecto en desarrollo 
se usa los siguientes comandos: 

    - docker-compose -f local.yml build
    - docker-compose -f local.yml up -d
    
Para forzar el reflejo de cambios en imagenes se utiliza el siguiente comando:

    - docker-compose -f local.yml build --no-cache
    
### AMBIENTE DE PRODUCCION
Para construir la imagen y ejecutar el proyecto en produccion 
se usa los siguientes comandos:

    - docker-compose -f production.yml build
    - docker-compose -f production.yml up -d